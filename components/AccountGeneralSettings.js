import React from "react";
import * as Yup from "yup";
import { Formik } from "formik";
import {
  Box,
  Button,
  Card,
  CardContent,
  Divider,
  FormHelperText,
  Grid,
  TextField,
} from "@material-ui/core";
import useAuth from "../src/hooks/useAuth";
import wait from "../src/utils/wait";

const AccountGeneralSettings = (props) => {
  const { data } = props;
  // const { enqueueSnackbar } = useSnackbar();

  return (
    <Grid container spacing={3} {...props}>
      <Grid item xs={12}>
        <Formik
          enableReinitialize
          initialValues={{
            storeName: data.StoreName,
            storeLink: data.StoreLink,
            yelpLink: data.YelpLink,
            email: data.Email,
            firstName: data.FirstName,
            lastName: data.LastName,
            address: data.Address,
            city: data.City,
            state: data.State,
            zip: data.Zip,
            submit: null,
          }}
          validationSchema={Yup.object().shape({
            storeName: Yup.string().max(255).required(),
            storeLink: Yup.string().max(255).required(),
            yelpLink: Yup.string().max(255).required(),
            email: Yup.string().max(255).required(),
            firstName: Yup.string().max(255).required(),
            lastName: Yup.string().max(255).required(),
            address: Yup.string().max(255).required(),
            city: Yup.string().max(255).required(),
            state: Yup.string().max(255).required(),
            zip: Yup.string().max(5).required(),
          })}
          onSubmit={async (
            values,
            { resetForm, setErrors, setStatus, setSubmitting }
          ) => {
            try {
              // NOTE: Make API request
              await wait(200);
              resetForm();
              setStatus({ success: true });
              setSubmitting(false);
              // enqueueSnackbar("Profile updated", {
              //   anchorOrigin: {
              //     horizontal: "right",
              //     vertical: "top",
              //   },
              //   variant: "success",
              // });
            } catch (err) {
              console.error(err);
              setStatus({ success: false });
              setErrors({ submit: err.message });
              setSubmitting(false);
            }
          }}
        >
          {({
            errors,
            handleBlur,
            handleChange,
            handleSubmit,
            isSubmitting,
            touched,
            values,
          }) => (
            <form onSubmit={handleSubmit}>
              <Card>
                <Divider />
                <CardContent>
                  <Grid container spacing={4}>
                    <Grid item md={6} xs={12}>
                      <TextField
                        error={Boolean(touched.storeName && errors.storeName)}
                        fullWidth
                        helperText={touched.storeName && errors.storeName}
                        label="Store Name"
                        name="storeName"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.storeName}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        error={Boolean(touched.storeLink && errors.storeLink)}
                        fullWidth
                        helperText={
                          touched.storeLink && errors.storeLink
                            ? errors.storeLink
                            : "We will use this storeLink to advertisement"
                        }
                        label="Store Link"
                        name="storeLink"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        required
                        type="storeLink"
                        value={values.storeLink}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        error={Boolean(touched.yelpLink && errors.yelpLink)}
                        fullWidth
                        helperText={touched.yelpLink && errors.yelpLink}
                        label="Yelp Link"
                        name="yelpLink"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.yelpLink}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        error={Boolean(touched.email && errors.email)}
                        fullWidth
                        helperText={touched.email && errors.email}
                        label="Email"
                        name="email"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.email}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        error={Boolean(touched.firstName && errors.firstName)}
                        fullWidth
                        helperText={touched.firstName && errors.firstName}
                        label="First Name"
                        name="firstName"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.firstName}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        error={Boolean(touched.lastName && errors.lastName)}
                        fullWidth
                        helperText={touched.lastName && errors.lastName}
                        label="Last Name"
                        name="lastName"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.lastName}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        error={Boolean(touched.address && errors.address)}
                        fullWidth
                        helperText={touched.address && errors.address}
                        label="Address"
                        name="address"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.address}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        error={Boolean(touched.state && errors.state)}
                        fullWidth
                        helperText={touched.state && errors.state}
                        label="State/Region"
                        name="state"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.state}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        error={Boolean(touched.city && errors.city)}
                        fullWidth
                        helperText={touched.city && errors.city}
                        label="City"
                        name="city"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.city}
                        variant="outlined"
                      />
                    </Grid>
                    <Grid item md={6} xs={12}>
                      <TextField
                        error={Boolean(touched.zipcode && errors.zipcode)}
                        fullWidth
                        helperText={touched.zipcode && errors.zipcode}
                        label="Zipcode"
                        name="zipcode"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        value={values.zipcode}
                        variant="outlined"
                      />
                    </Grid>
                  </Grid>
                  {errors.submit && (
                    <Box sx={{ mt: 3 }}>
                      <FormHelperText error>{errors.submit}</FormHelperText>
                    </Box>
                  )}
                </CardContent>
                <Divider />
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "flex-end",
                    p: 2,
                  }}
                >
                  <Button
                    color="primary"
                    disabled={isSubmitting}
                    type="submit"
                    variant="contained"
                  >
                    Save Changes
                  </Button>
                </Box>
              </Card>
            </form>
          )}
        </Formik>
      </Grid>
    </Grid>
  );
};

export default AccountGeneralSettings;
