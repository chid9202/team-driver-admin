export const getThirtyDays = () => {
  // Get the current date
  var myDate = new Date();
  var nowY = myDate.getFullYear();
  var nowM = myDate.getMonth() + 1;
  var nowD = myDate.getDate();
  var enddateStr =
    nowY +
    "-" +
    (nowM < 10 ? "0" + nowM : nowM) +
    "-" +
    (nowD < 10 ? "0" + nowD : nowD); // Current date
  var enddate = new Date(enddateStr);
  // Get the date thirty days ago
  var lw = new Date(myDate - 1000 * 60 * 60 * 24 * 30); // The last number 30 can be changed, meaning 30 days
  var lastY = lw.getFullYear();
  var lastM = lw.getMonth() + 1;
  var lastD = lw.getDate();
  var startdateStr =
    lastY +
    "-" +
    (lastM < 10 ? "0" + lastM : lastM) +
    "-" +
    (lastD < 10 ? "0" + lastD : lastD); // The date before thirty days
  var startDate = new Date(startdateStr);

  const dateList = [];
  while (true) {
    startDate.setDate(startDate.getDate() + 1);
    if (startDate.getTime() <= enddate.getTime()) {
      dateList.push(
        `${startDate.getFullYear()}-${
          startDate.getMonth() + 1
        }-${startDate.getDate()}`
      );
    } else {
      break;
    }
  }
  return dateList;
};

export const formatDate = (date) => {
  date = new Date(date.setMonth(date.getMonth() + 1));
  let result = "";
  result = date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate();
  return result;
};
