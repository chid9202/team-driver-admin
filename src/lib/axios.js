import axios from "axios";
import AxiosMockAdapter from "axios-mock-adapter";
import { airTableConfig } from "../config";
import React from "react";

const axiosInstance = axios.create();

axiosInstance.interceptors.response.use(
  (response) => response,
  (error) =>
    Promise.reject(
      (error.response && error.response.data) || "Something went wrong"
    )
);
axiosInstance.defaults.headers.common = {
  Authorization: `Bearer ${airTableConfig}`,
};

export const mock = new AxiosMockAdapter(axiosInstance, { delayResponse: 0 });

export default axiosInstance;
