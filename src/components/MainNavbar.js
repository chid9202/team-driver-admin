import PropTypes from "prop-types";
import {
  AppBar,
  Box,
  Button,
  Chip,
  Divider,
  IconButton,
  Link,
  Toolbar,
} from "@material-ui/core";
import MenuIcon from "../icons/Menu";
import Logo from "./Logo";
import React from "react";

const MainNavbar = (props) => {
  const { onSidebarMobileOpen } = props;

  return (
    <AppBar
      elevation={0}
      sx={{
        backgroundColor: "background.paper",
        color: "text.secondary",
      }}
    >
      <Toolbar sx={{ minHeight: 64 }}>
        <IconButton color="inherit" onClick={onSidebarMobileOpen}>
          <MenuIcon fontSize="small" />
        </IconButton>

        <Box sx={{ flexGrow: 1 }} />
        <Link
          color="textSecondary"
          to="/browse"
          underline="none"
          variant="body1"
        >
          Browse Components
        </Link>
        <Chip
          color="primary"
          label="NEW"
          size="small"
          sx={{
            maxHeight: 20,
            ml: 1,
            mr: 2,
          }}
        />
        <Link color="textSecondary" to="/docs" underline="none" variant="body1">
          Documentation
        </Link>
        <Divider
          orientation="vertical"
          sx={{
            height: 32,
            mx: 2,
          }}
        />
        <Button
          color="primary"
          component="a"
          href="https://material-ui.com/store/items/devias-kit-pro"
          size="small"
          target="_blank"
          variant="contained"
        >
          Get the kit
        </Button>
      </Toolbar>
      <Divider />
    </AppBar>
  );
};

MainNavbar.propTypes = {
  onSidebarMobileOpen: PropTypes.func,
};

export default MainNavbar;
