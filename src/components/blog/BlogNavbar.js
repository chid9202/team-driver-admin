import React from "react";
import { AppBar, Box, Divider, Hidden, Link, Toolbar } from "@material-ui/core";
import Logo from "../Logo";

const BlogNavbar = (props) => (
  <AppBar elevation={0} sx={{ backgroundColor: "background.paper" }} {...props}>
    <Toolbar sx={{ minHeight: 64 }}>
      <Logo
        sx={{
          height: 40,
          width: 40,
        }}
      />
      <Box sx={{ flexGrow: 1 }} />
      <Hidden mdDown>
        <Link color="textSecondary" to="/" underline="none" variant="body1">
          Home
        </Link>
        <Link
          color="textPrimary"
          to="/blog"
          underline="none"
          sx={{ mx: 2 }}
          variant="body1"
        >
          Blog
        </Link>
        <Link
          color="textSecondary"
          to="/contact"
          underline="none"
          variant="body1"
        >
          Contact Us
        </Link>
      </Hidden>
    </Toolbar>
    <Divider />
  </AppBar>
);

export default BlogNavbar;
