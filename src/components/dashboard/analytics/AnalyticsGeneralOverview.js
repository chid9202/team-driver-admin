import React from "react";
import dynamic from "next/dynamic";
const Chart = dynamic(() => import("react-apexcharts"), { ssr: false });

import { Box, Card, Grid, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";

const LineChart = () => {
  const theme = useTheme();

  const chart = {
    options: {
      chart: {
        background: "transparent",
        toolbar: {
          show: false,
        },
        zoom: {
          enabled: false,
        },
      },
      colors: ["#7783DB"],
      dataLabels: {
        enabled: false,
      },
      grid: {
        show: false,
      },
      stroke: {
        width: 3,
      },
      theme: {
        mode: theme.palette.mode,
      },
      tooltip: {
        enabled: false,
      },
      xaxis: {
        labels: {
          show: false,
        },
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
      },
      yaxis: {
        show: false,
      },
    },
    series: [
      {
        data: [0, 60, 30, 60, 0, 30, 10, 30, 0],
      },
    ],
  };

  return <Chart type="line" width={120} {...chart} />;
};

const BarChart = () => {
  const theme = useTheme();

  const chart = {
    options: {
      chart: {
        background: "transparent",
        toolbar: {
          show: false,
        },
        zoom: {
          enabled: false,
        },
      },
      colors: ["#7783DB"],
      dataLabels: {
        enabled: false,
      },
      grid: {
        show: false,
      },
      states: {
        normal: {
          active: {
            filter: {
              type: "none",
            },
          },
          filter: {
            type: "none",
            value: 0,
          },
        },
      },
      stroke: {
        width: 0,
      },
      theme: {
        mode: theme.palette.mode,
      },
      tooltip: {
        enabled: false,
      },
      xaxis: {
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
        labels: {
          show: false,
        },
      },
      yaxis: {
        show: false,
      },
    },
    series: [
      {
        data: [10, 20, 30, 40, 50, 60, 5],
      },
    ],
  };

  return <Chart type="bar" width={120} {...chart} />;
};

const AnalyticsGeneralOverview = ({ urlVisitCount, deliveryCount }) => (
  <Grid container spacing={2} py={3}>
    <Grid item md={6} sm={6} xs={12}>
      <Card>
        <Box
          sx={{
            alignItems: "center",
            display: "flex",
            justifyContent: "space-between",
            p: 3,
          }}
        >
          <div>
            <Typography color="textPrimary" variant="subtitle2">
              Delivery in 30 days
            </Typography>
            <Typography color="textPrimary" sx={{ mt: 1 }} variant="h4">
              {deliveryCount}
            </Typography>
          </div>
          <LineChart />
        </Box>
      </Card>
    </Grid>
    <Grid item md={6} sm={6} xs={12}>
      <Card>
        <Box
          sx={{
            alignItems: "center",
            display: "flex",
            justifyContent: "space-between",
            p: 3,
          }}
        >
          <div>
            <Typography color="textPrimary" variant="subtitle2">
              URL Visit in 30 Days
            </Typography>
            <Typography color="textPrimary" sx={{ mt: 1 }} variant="h5">
              {urlVisitCount}
            </Typography>
          </div>
          <LineChart />
        </Box>
      </Card>
    </Grid>
  </Grid>
);

export default AnalyticsGeneralOverview;
