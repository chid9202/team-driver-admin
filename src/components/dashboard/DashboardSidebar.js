import React from "react";
import PropTypes from "prop-types";
import { Avatar, Box, Divider, Drawer, Typography } from "@material-ui/core";
import ChartPieIcon from "../../icons/ChartPie";
import ChartSquareBarIcon from "../../icons/ChartSquareBar";
import UserIcon from "../../icons/User";
import Mail from "../../icons/Mail";
import NavSection from "../NavSection";
import { useUser } from "@auth0/nextjs-auth0";

const sections = [
  {
    title: "",
    items: [
      {
        title: "Dashboard",
        path: "/dashboard",
        icon: <ChartSquareBarIcon />,
      },
      {
        title: "Invoices",
        path: "/invoices",
        icon: <ChartPieIcon />,
      },
      {
        title: "Help",
        path: "/help",
        icon: <Mail />,
      },
      {
        title: "Account",
        path: "/account",
        icon: <UserIcon />,
      },
    ],
  },
];

const DashboardSidebar = (props) => {
  const { onMobileClose, openMobile } = props;
  const { user, isLoading } = useUser();
  let storeName = "";
  if (typeof window !== "undefined") {
    const store = JSON.parse(localStorage.getItem("store"));
    storeName = store && store.fields && store.fields.StoreName;
  }
  const content = (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        height: "100%",
      }}
    >
      {/* <Scrollbar> */}
      {/* <Hidden lgUp>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              p: 2,
            }}
          >
            <Logo
              sx={{
                height: 40,
                width: 40,
              }}
            />
          </Box>
        </Hidden> */}
      <Box sx={{ p: 2 }}>
        <Box
          sx={{
            alignItems: "center",
            backgroundColor: "background.default",
            borderRadius: 1,
            display: "flex",
            overflow: "hidden",
            p: 2,
          }}
        >
          {!isLoading && (
            <Avatar
              src={user.picture}
              sx={{
                cursor: "pointer",
                height: 48,
                width: 48,
              }}
            />
          )}
          <Box sx={{ ml: 2 }}>
            <Typography color="textPrimary" variant="subtitle2">
              {!isLoading && user.name}
            </Typography>
            <Typography
              color="textSecondary"
              variant="body2"
              suppressHydrationWarning
            >
              {storeName}
            </Typography>
          </Box>
        </Box>
      </Box>
      <Divider />
      <Box sx={{ p: 2 }}>
        {sections.map((section, idx) => (
          <NavSection
            key={`nav-section-${idx}`}
            pathname={`/`}
            sx={{
              "& + &": {
                mt: 3,
              },
            }}
            {...section}
          />
        ))}
      </Box>
      <Divider />
    </Box>
  );

  return (
    <>
      <Drawer
        anchor="left"
        open
        PaperProps={{
          sx: {
            backgroundColor: "background.paper",
            height: "calc(100% - 64px) !important",
            top: "64px !Important",
            width: 280,
          },
        }}
        variant="persistent"
      >
        {content}
      </Drawer>
    </>
  );
};

DashboardSidebar.propTypes = {
  onMobileClose: PropTypes.func,
  openMobile: PropTypes.bool,
};

export default DashboardSidebar;
