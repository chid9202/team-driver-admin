import React from "react";
import PropTypes from "prop-types";
import Link from "next/link";
import { AppBar, Box, Toolbar, Button, Typography } from "@material-ui/core";
import { experimentalStyled } from "@material-ui/core/styles";

const DashboardNavbar = (props) => {
  const { onSidebarMobileOpen, ...other } = props;
  return (
    <AppBar position="fixed">
      <Toolbar>
        <Box
          sx={{
            flexGrow: "1",
          }}
        >
          <Typography>Suga-App Admin</Typography>
        </Box>
        <Box
          sx={{
            justifyContent: "flex-end",
            flexDirection: "row",
          }}
        >
          <Link href={"/api/auth/logout"}>
            <Button color="inherit" variant="outlined">
              Logout
            </Button>
          </Link>
        </Box>
      </Toolbar>
    </AppBar>
  );
};

DashboardNavbar.propTypes = {
  onSidebarMobileOpen: PropTypes.func,
};

export default DashboardNavbar;
