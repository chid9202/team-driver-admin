import React from "react";
import PropTypes from "prop-types";
import { experimentalStyled } from "@material-ui/core/styles";
import DashboardLayout from "./dashboard/DashboardLayout";

const MainLayoutRoot = experimentalStyled("div")(({ theme }) => ({
  backgroundColor: theme.palette.background.default,
  height: "100%",
  paddingTop: 64,
}));

const MainLayout = ({ children }) => {
  return (
    <MainLayoutRoot>
      <DashboardLayout>{children}</DashboardLayout>
    </MainLayoutRoot>
  );
};

MainLayout.propTypes = {
  children: PropTypes.node,
};

export default MainLayout;
