import React from "react";
import { UserProvider } from "@auth0/nextjs-auth0";
import { SnackbarProvider } from "notistack";
import MainLayout from "../src/components/MainLayout";

export default function App({ Component, pageProps }) {
  switch (Component.name) {
    case "Welcome":
      return <Component {...pageProps} />;
    default:
      return (
        <UserProvider>
          <MainLayout>
            <SnackbarProvider dense maxSnack={3}>
              <Component {...pageProps} />
            </SnackbarProvider>
          </MainLayout>
        </UserProvider>
      );
  }
}
