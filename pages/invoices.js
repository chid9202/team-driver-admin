import React from "react";
import { Box, Button, Container, Grid, Typography } from "@material-ui/core";
import { getSession } from "@auth0/nextjs-auth0";
import axios from "axios";
import useSettings from "../src/hooks/useSettings";
import { InvoiceListTable } from "../src/components/dashboard/invoice";

import { subHours, subDays } from "date-fns";
const now = new Date();

const invoices = [
  {
    id: "5ecb868d0f437390ef3ac62c",
    currency: "$",
    customer: {
      email: "contact@anahenisky.io",
      name: "Ana Henisky",
    },
    issueDate: subHours(now, 1).getTime(),
    status: "paid",
    totalAmount: 55.5,
  },
  {
    id: "5ecb868ada8deedee0638502",
    currency: "$",
    customer: {
      email: "sales@matt-jason.com",
      name: "Matt Jason",
    },
    issueDate: subDays(subHours(now, 5), 2).getTime(),
    status: "pending",
    totalAmount: 253.76,
  },
  {
    id: "5ecb868700aba84d0f1c0e48",
    currency: "$",
    customer: {
      email: "support@terrythomas.io",
      name: "Terry Thomas",
    },
    issueDate: subDays(subHours(now, 4), 6).getTime(),
    status: "canceled",
    totalAmount: 781.5,
  },
  {
    id: "5ecb8682038e1ddf4e868764",
    currency: "$",
    customer: {
      email: "contact@triv-shopper.co.uk",
      name: "Triv Shopper",
    },
    issueDate: subDays(subHours(now, 2), 15).getTime(),
    status: "paid",
    totalAmount: 96.64,
  },
];

const Invoices = (props) => {
  console.log(props);
  const { settings } = useSettings();
  return (
    <>
      <Box
        sx={{
          minHeight: "100%",
          py: 8,
        }}
      >
        <Container maxWidth={settings.compact ? "xl" : false}>
          <Grid container justifyContent="space-between" spacing={3}>
            <Grid item>
              <Typography color="textPrimary" variant="h5">
                Invoice List
              </Typography>
            </Grid>
            {/* <Grid item>
              <Box sx={{ m: -1 }}>
                <Button
                  color="primary"
                  startIcon={<PlusIcon fontSize="small" />}
                  sx={{ m: 1 }}
                  variant="contained"
                >
                  New Invoice
                </Button>
              </Box>
            </Grid> */}
          </Grid>
          <Box sx={{ mt: 3 }}>
            <InvoiceListTable invoices={invoices} />
          </Box>
        </Container>
      </Box>
    </>
  );
};

// export async function getServerSideProps({ req, res }) {
//   // user is not logged in
//   const session = await getSession(req, res);
//   if (!session || !session.user || !session.user.email) {
//     return {
//       props: {},
//       redirect: {
//         destination: "/api/auth/login",
//         permanent: false,
//       },
//     };
//   }
//   const result = await axios.get(`http://localhost:3000/api/dashboard`, {
//     params: { email: session.user.email },
//   });

//   return {
//     props: { data: result && result.data },
//   };
// }

export default Invoices;
