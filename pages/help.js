import React, { useState } from "react";
import {
  Button,
  Box,
  Container,
  Grid,
  TextField,
  Typography,
} from "@material-ui/core";
import axios from "axios";
import { getSession } from "@auth0/nextjs-auth0";
import useSettings from "../src/hooks/useSettings";
import { useSnackbar } from "notistack";

const tabs = [{ label: "General", value: "general" }];

const Help = (props) => {
  const [message, setMessage] = useState("");
  const { enqueueSnackbar } = useSnackbar();
  const { settings } = useSettings();

  const handleClick = async () => {
    await axios
      .post("/api/help", {
        content: message,
        store: props.user.id,
      })
      .then((res) => {
        setMessage("");
        if (res.status === 200) {
          enqueueSnackbar("Message has been sent.", {
            anchorOrigin: {
              horizontal: "right",
              vertical: "top",
            },
            variant: "success",
          });
        } else {
          enqueueSnackbar("Something went wrong.", {
            anchorOrigin: {
              horizontal: "right",
              vertical: "top",
            },
            variant: "success",
          });
        }
      });
  };

  return (
    <>
      <Box
        sx={{
          minHeight: "100%",
          py: 8,
        }}
      >
        <Container maxWidth={settings.compact ? "xl" : false}>
          <Grid container justifyContent="space-between" spacing={3}>
            <Grid item>
              <Typography color="textPrimary" variant="h5">
                Help
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Typography
                color="textPrimary"
                sx={{ mb: 1 }}
                variant="subtitle2"
              >
                Message
              </Typography>
              <TextField
                fullWidth
                name="message"
                required
                multiline
                rows={6}
                value={message}
                variant="outlined"
                onChange={(e) => setMessage(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  mt: 3,
                  width: "120px",
                }}
              >
                <Button
                  color="primary"
                  fullWidth
                  size="large"
                  variant="contained"
                  onClick={handleClick}
                >
                  Send
                </Button>
              </Box>
            </Grid>
          </Grid>
        </Container>
      </Box>
    </>
  );
};

export async function getServerSideProps({ req, res }) {
  // user is not logged in
  const session = await getSession(req, res);
  if (!session || !session.user || !session.user.email) {
    return {
      props: {},
      redirect: {
        destination: "/api/auth/login",
        permanent: false,
      },
    };
  }

  const validation = await axios.get(
    `${process.env.API_URL}/api/store/validation`,
    {
      params: { email: session.user.email },
    }
  );

  return {
    props: { user: validation.data },
  };
}

export default Help;
