import base from "../../../src/lib/airtable";

export default (req, res) => {
  const { method } = req;
  switch (method) {
    case "GET":
      handleGet(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
};

const handleGet = (req, res) => {
  base("Driver").find(req.query.id, function (err, record) {
    if (err) {
      console.error(err);
      res.status(400).end(`ID not found`);
      return;
    }
    res.status(200).json(record._rawJson);
    return;
  });
};
