import base from "../../../src/lib/airtable";

export default (req, res) => {
  const { method } = req;
  switch (method) {
    case "GET":
      handleGet(req, res);
      break;
    case "POST":
      handlePost(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
};

const handleGet = (req, res) => {
  const result = [];
  base("Driver")
    .select({
      maxRecords: 20,
      view: "default",
    })
    .eachPage(
      function page(records, fetchNextPage) {
        records.forEach(function (record) {
          result.push({
            ...record._rawJson,
          });
        });

        fetchNextPage();
      },
      function done(err) {
        if (err) {
          console.error(err);
          res.status(500).json(err);
          return;
        }
        res.status(200).json(result);
        return;
      }
    );
};

const handlePost = (req, res) => {
  const body = req.body;
  const result = [];
  // validate input

  base("Driver").create(
    [
      {
        fields: {
          FirstName: req.body.FirstName,
          LastName: req.body.LastName,
          Address: req.body.Address,
          City: req.body.City,
          State: req.body.State,
        },
      },
    ],
    function (err, records) {
      if (err) {
        console.error(err);
        res.status(500).json(err);
        return;
      }
      records.forEach(function (record) {
        result.push({
          ...record._rawJson,
        });
      });
      res.status(200).json(result);
      return;
    }
  );
};
