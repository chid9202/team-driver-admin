import base from "../../../src/lib/airtable";

export default (req, res) => {
  const { method } = req;
  switch (method) {
    case "GET":
      handleGet(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
};

const handleGet = (req, res) => {
  base("Driver").find(req.query.id, function (err, record) {
    if (err) {
      console.error(err);
      res.status(400).end(`ID not found`);
      return;
    }
    const data = { ...record._rawJson };
    const fields = data.fields;
    const result = {};

    // get delivery
    let delivery = [];
    if (fields.Delivery && fields.Delivery.length > 0) {
      delivery = fields.Delivery.map((_, idx) => ({
        address: fields.Delivery_Address[idx],
        city: fields.Delivery_City[idx],
        state: fields.Delivery_State[idx],
        created: fields.Delivery_Created[idx],
      }));
    }
    result.delivery = delivery;

    // get stock
    let itemIndex = undefined;
    if (fields.Item_IsActive && fields.Item_IsActive.length > 0) {
      itemIndex = fields.Item_IsActive.findIndex((x) => x === true);
    }
    if (itemIndex !== undefined) {
      result.stock = {
        currentAmount: fields.Item_InStock[itemIndex],
        initialAmount: fields.Item_InitialStock[itemIndex],
      };
    }
    res.status(200).json(result);
    return;
  });
};
