import base from "../../src/lib/airtable";
import { getThirtyDays, formatDate } from "../../src/utils/util";

export default (req, res) => {
  const { method } = req;
  switch (method) {
    case "GET":
      handleGet(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
};

const days = getThirtyDays();

const getFormula = (email) =>
  `AND(IS_AFTER((Created), "${days[0]}"), IS_BEFORE((Created), "${
    days[days.length - 1]
  }"), {Email (from Store)} = '${email}')`;

const handleGet = async (req, res) => {
  const result = {
    deliveryCount: undefined,
    urlVisitCount: undefined,
    lastDays: {},
    urlVisit: {},
  };

  const days = getThirtyDays();

  days.forEach((date) => (result.lastDays[date] = 0));
  days.forEach((date) => (result.urlVisit[date] = 0));

  Promise.all([
    processDelivery(result.lastDays, req.query.email),
    processUrlVisit(result.urlVisit, req.query.email),
  ]).then((values) => {
    res.status(200).json({ ...values[0], ...values[1] });
  });
};

const processDelivery = async (lastDays, email) => {
  let deliveryCounter = 0;
  const result = {
    deliveryCount: undefined,
    deliveryHistory: [],
    lastDays,
  };

  return new Promise((resolve, reject) => {
    base("Delivery")
      .select({
        filterByFormula: getFormula(email),
        fields: [
          "Address",
          "City",
          "State",
          "Driver_FirstName",
          "Driver_LastName",
          "Created",
        ],
      })
      .eachPage(
        function page(records, fetchNextPage) {
          records.forEach(function (record) {
            const recordData = record._rawJson;
            result.deliveryHistory.push(recordData.fields);
            const date = new Date(recordData.fields.Created);
            const dateStr = formatDate(date);
            const newValue = result.lastDays[dateStr]
              ? result.lastDays[dateStr]
              : 0;
            result.lastDays[dateStr] = newValue + 1;
            deliveryCounter++;
          });
          fetchNextPage();
        },
        function done(err) {
          if (err) {
            console.error(err);
            return;
          }
          result.deliveryCount = deliveryCounter;
          resolve(result);
        }
      );
  });
};

const processUrlVisit = async (urlVisit, email) => {
  let urlVisitCounter = 0;
  const result = {
    urlVisitCount: undefined,
    urlVisit,
  };

  return new Promise((resolve, reject) => {
    base("QRCodeViewHistory")
      .select({
        filterByFormula: getFormula(email),
        fields: ["Created"],
      })
      .eachPage(
        function page(records, fetchNextPage) {
          records.forEach(function (record) {
            const recordData = record._rawJson;
            const date = new Date(recordData.fields.Created);
            const dateStr = formatDate(date);
            const newValue = result.urlVisit[dateStr]
              ? result.urlVisit[dateStr]
              : 0;
            result.urlVisit[dateStr] = newValue + 1;
            urlVisitCounter++;
          });
          fetchNextPage();
        },
        function done(err) {
          if (err) {
            console.error(err);
            return;
          }
          result.urlVisitCount = urlVisitCounter;
          resolve(result);
        }
      );
  });
};
