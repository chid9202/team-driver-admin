import base from "../../../src/lib/airtable";

export default (req, res) => {
  const { method } = req;
  switch (method) {
    case "GET":
      handleGet(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
};

const handleGet = (req, res) => {
  let result = [];
  base("Item")
    .select({
      filterByFormula: `AND(IsActive, {Email (from Store)} = 'chid9202@gmail.com')`,
      fields: ["Name", "InStock", "InitialStock"],
    })
    .eachPage(
      function page(records, fetchNextPage) {
        records.forEach(function (record) {
          result.push({ ...record._rawJson });
        });
        fetchNextPage();
      },
      function done(err) {
        if (err) {
          res.status(500).json(result);
          console.error(err);
          return;
        }
        res.status(200).json(result);
        return;
      }
    );
};
