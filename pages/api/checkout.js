import base from "../../src/lib/airtable";

export default (req, res) => {
  const { method } = req;
  switch (method) {
    case "GET":
      handleGet(req, res);
      break;
    case "POST":
      handlePost(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
};

const handlePost = (req, res) => {
  const body = req.body;
  let currentInStock = undefined;
  let itemId = undefined;
  if (!body.email) {
    res.status(400).json({ msg: "invalid input" });
    return;
  }
  // get current InStock
  base("Item")
    .select({
      filterByFormula: `AND(IsActive, {Email (from Store)} = '${body.email}')`,
      maxRecords: 1,
    })
    .eachPage(
      function page(records, fetchNextPage) {
        records.forEach(function (record) {
          itemId = record.id;
          currentInStock = record.get("InStock");
        });
        fetchNextPage();
      },
      function done(err) {
        if (err) {
          res.status(500).json({ msg: err });
          console.error(err);
          return;
        }
        updateInStock(itemId, currentInStock, req, res);
      }
    );
};

const updateInStock = (itemId, currentInStock, req, res) => {
  let result = {};
  base("Item").update(
    [
      {
        id: itemId,
        fields: {
          InStock: currentInStock - 1,
        },
      },
    ],
    function (err, records) {
      if (err) {
        console.error(err);
        res.status(500);
        return;
      }

      res.status(200).json(result);
      return;
    }
  );
};
