import base from "../../src/lib/airtable";

export default (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      handlePost(req, res);
      break;
    default:
      res.status(405).end(`Method ${method} Not Allowed`);
  }
};

const handlePost = (req, res) => {
  base("Help").create(
    [
      {
        fields: {
          Store: [req.body.store],
          Content: req.body.content,
        },
      },
    ],
    function (err, records) {
      if (err) {
        console.error(err);
        res.status(500).json(err);
        return;
      }

      res.status(200).json();
      return;
    }
  );
};
