import base from "../../../src/lib/airtable";

export default (req, res) => {
  const { method } = req;
  switch (method) {
    case "GET":
      handleGet(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
};

const handleGet = (req, res) => {
  if (!req.query.email) {
    res.status(400).end("Invalid Input");
    return;
  }
  let result = undefined;
  base("Store")
    .select({
      filterByFormula: `{Email} = "${req.query.email}"`,
      maxRecords: 1,
    })
    .eachPage(
      function page(records, fetchNextPage) {
        records.forEach(function (record) {
          result = record._rawJson;
        });

        fetchNextPage();
      },
      function done(err) {
        if (err) {
          console.error(err);
          res.status(500);
          res.end();
        }
        res.status(200).json(result);
        res.end();
      }
    );
};
