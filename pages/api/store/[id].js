import base from "../../../src/lib/airtable";

export default (req, res) => {
  const { method } = req;
  switch (method) {
    case "GET":
      handleGet(req, res);
      break;
    case "PUT":
      handlePut(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${method} Not Allowed`);
  }
};

const handleGet = (req, res) => {
  base("Store").find(req.query.id, function (err, record) {
    if (err) {
      console.error(err);
      res.status(400).end(`Store ID not found`);
      return;
    }
    res.status(200).json(record._rawJson);
    res.end();
  });
};

const handlePut = (req, res) => {
  const body = req.body;
  const result = [];
  base("Store").update(
    [
      {
        id: req.query.id,
        fields: {
          StoreName: req.body.StoreName,
          StoreLink: req.body.StoreName,
          YelpLInk: req.body.YelpLInk,
          Address: req.body.Address,
          City: req.body.City,
          State: req.body.State,
          Zip: req.body.Zip,
        },
      },
    ],
    function (err, records) {
      if (err) {
        console.error(err);
        return;
      }
      records.forEach(function (record) {
        result.push({
          ...record._rawJson,
        });
        res.status(200).json(result);
      });
    }
  );
};
