import React from "react";
import { Box, Container, Grid, Typography } from "@material-ui/core";
import axios from "axios";
import { getSession } from "@auth0/nextjs-auth0";
import AccountGeneralSettings from "../components/AccountGeneralSettings";
import useSettings from "../src/hooks/useSettings";

const Account = (props) => {
  const { settings } = useSettings();

  return (
    <>
      <Box
        sx={{
          minHeight: "100%",
          py: 8,
        }}
      >
        <Container maxWidth={settings.compact ? "xl" : false}>
          <Grid container justifyContent="space-between" spacing={3}>
            <Grid item>
              <Typography color="textPrimary" variant="h5">
                Account
              </Typography>
            </Grid>
            <Grid item>
              <AccountGeneralSettings data={props.data} />
            </Grid>
          </Grid>
        </Container>
      </Box>
    </>
  );
};

export async function getServerSideProps({ req, res }) {
  // user is not logged in
  const session = await getSession(req, res);
  if (!session || !session.user || !session.user.email) {
    return {
      props: {},
      redirect: {
        destination: "/api/auth/login",
        permanent: false,
      },
    };
  }

  const validation = await axios.get(
    `${process.env.API_URL}/api/store/validation`,
    {
      params: { email: session.user.email },
    }
  );

  // user is valid.. redirect to dashboard
  return {
    props: { data: validation.data.fields },
  };
  // show invalid user
}

export default Account;
