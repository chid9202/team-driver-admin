import React, { useEffect } from "react";
import { useRouter } from "next/router";
import axios from "axios";
import { getSession } from "@auth0/nextjs-auth0";

const Home = (props) => {
  const router = useRouter();
  if (typeof window !== "undefined") {
    localStorage.setItem("store", JSON.stringify(props));
  }

  useEffect(() => {
    if (Object.keys(props).length > 0) {
      router.push("/dashboard");
    } else {
      router.push("/welcome");
    }
  }, []);

  return <div></div>;
};

export async function getServerSideProps({ req, res }) {
  // user is not logged in
  const session = await getSession(req, res);
  if (!session || !session.user || !session.user.email) {
    return {
      props: {},
      redirect: {
        destination: "/api/auth/login",
        permanent: false,
      },
    };
  }

  // const validation = await axios;
  const validation = await axios.get(
    `${process.env.API_URL}/api/store/validation`,
    {
      params: { email: session.user.email },
    }
  );
  return {
    props: validation && validation.data,
  };
}

export default Home;
