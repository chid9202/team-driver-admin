import React from "react";
import { Box, Container, Grid, Typography } from "@material-ui/core";
import { getSession } from "@auth0/nextjs-auth0";
import axios from "axios";
import useSettings from "../src/hooks/useSettings";
import {
  AnalyticsGeneralOverview,
  AnalyticsTrafficSources,
} from "../src/components/dashboard/analytics";
import { OverviewLatestTransactions } from "../src/components/dashboard/overview";

const Dashboard = (props) => {
  console.log(props);
  const { settings } = useSettings();
  return (
    <>
      <Box
        sx={{
          minHeight: "100%",
          py: 8,
        }}
      >
        <Container maxWidth={settings.compact ? "xl" : false}>
          <Grid container justifyContent="space-between" spacing={3}>
            <Grid item>
              <Typography color="textPrimary" variant="h5">
                Dashboard
              </Typography>
            </Grid>
          </Grid>
          <AnalyticsGeneralOverview
            deliveryCount={props.data.deliveryCount}
            urlVisitCount={props.data.urlVisitCount}
          />
          <Grid container spacing={2}>
            <Grid item md={6} sm={6} xs={12}>
              <AnalyticsTrafficSources
                lastDays={props.data.lastDays}
                urlVisit={props.data.urlVisit}
              />
            </Grid>
            <Grid item md={6} sm={6} xs={12}>
              <OverviewLatestTransactions
                history={props.data.deliveryHistory}
              />
            </Grid>
          </Grid>
        </Container>
      </Box>
    </>
  );
};

export async function getServerSideProps({ req, res }) {
  // user is not logged in
  const session = await getSession(req, res);
  if (!session || !session.user || !session.user.email) {
    return {
      props: {},
      redirect: {
        destination: "/api/auth/login",
        permanent: false,
      },
    };
  }
  const result = await axios.get(`${process.env.API_URL}/api/dashboard`, {
    params: { email: session.user.email },
  });

  return {
    props: { data: result && result.data },
  };
}

export default Dashboard;
