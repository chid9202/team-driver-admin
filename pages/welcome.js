import React from "react";
import { Box, Container, Typography } from "@material-ui/core";
import DashboardNavbar from "../src/components/dashboard/DashboardNavbar";

const Welcome = () => {
  return (
    <>
      <DashboardNavbar
        onSidebarMobileOpen={() => setIsSidebarMobileOpen(true)}
      />
      <Box
        sx={{
          alignItems: "center",
          backgroundColor: "background.paper",
          display: "flex",
          minHeight: "100%",
          px: 3,
          py: "80px",
        }}
      >
        <Container maxWidth="lg">
          <Typography align="center" color="textPrimary" variant="h4">
            Welcome to Suga-App
          </Typography>
          <Typography
            align="center"
            color="textSecondary"
            sx={{ mt: 0.5 }}
            variant="subtitle2"
          >
            Our agent is working on your account and will verify soon!
          </Typography>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              mt: 6,
            }}
          >
            <Box
              alt="Under development"
              component="img"
              src={`/static/error/error401_light.svg`}
              sx={{
                height: "auto",
                maxWidth: "100%",
                width: 400,
              }}
            />
          </Box>
        </Container>
      </Box>
    </>
  );
};

export default Welcome;
